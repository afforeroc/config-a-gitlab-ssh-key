# Config a GitLab SSH key

## Tutorial to config a GitLab SSH key

1. Check if your system has a SSH client.
```
ssh -V
```

2. Generate a new SSH key for GitLab.
```
ssh-keygen -o -f ~/.ssh/id_rsa_gitlab
```

3.  Access into `.ssh/` folder and check that `id_rsa_gitlab` (private key) and `id_rsa_gitlab.pub` (public key) were created.
```
cd .ssh/
ls
```
> id_rsa_gitlab  id_rsa_gitlab.pub

4. Show the content of the public key and add it into GitLab [SSH Keys](https://gitlab.com/profile/keys) of User Settings section.
```
cat id_rsa_gitlab.pub
```

5. Finally, go to home folder and test your conection with GitLab using SSH.
```
cd ~
ssh -T git@gitlab.com
```
> Welcome to GitLab, @afforeroc!


## Reference information
* [GitLab Docs - GitLab and SSH keys](https://docs.gitlab.com/ee/ssh/)
* [ProCode Tv - Como configurar SSH Key en #Gitlab](https://www.youtube.com/watch?v=j-zmv-ITQb8)